#!/bin/bash

# MIKIZHA
# monthly
rsync -r -u /raid/common/Data/Mikizha-station/Meteo/mik_month/ /mnt/common/dan/meteo/LAI/mikizha/monthly
# daily
rsync -r -u /raid/common/Data/Mikizha-station/Meteo/met2/ /mnt/common/dan/meteo/LAI/mikizha/daily


# KARYMSHINA
# monthly
rsync -r -u /raid/common/Data/Karimshino-station/Meteo/kar_month/ /mnt/common/dan/meteo/LAI/karymshina/monthly
# daily
rsync -r -u /raid/common/Data/Karimshino-station/Meteo/met2/ /mnt/common/dan/meteo/LAI/karymshina/daily
