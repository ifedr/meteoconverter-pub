#!/usr/bin/env python3
"""\
Программа для заливки метео данных из текстового формата в базу данных.
./txt2db.py file station
file - путь к txt файлу;
station - станция (mik, kar, par), если не указана, то определяется автоматически из имени файла.
Автор fedr. Версия 1-20150310.
"""
import psycopg2 as db
import sys
from datetime import datetime


class TxtToDbLoader(object):
    def __init__(self, filepath, station=None):
        print("Открываем файл... ", end="")
        self.input_file = open(filepath, "rt", errors="ignore")
        print("ОК.")

        print("Подключаемся к базе... ", end="")
        sys.stdout.flush()
        self.con = db.connect(host="", database="", user="", password="")
        print("ОК.")
        self.con.autocommit = True
        self.cur = self.con.cursor()

        # read the header
        self.input_file.readline()
        line2 = self.input_file.readline()
        if "[m/s]" in line2:
            self.meters_pers_second = True
        else:
            self.meters_pers_second = False

        # define
        stations = {"mik": 1, "kar": 2, "par": 3}
        if station:
            self.st_id = stations[station]
        else:
            for k, v in stations.items():
                if k in self.input_file.name:
                    self.st_id = v
                    break

    _wdl = ['N', 'NtE', 'NNE', 'NEtN', 'NE', 'NEtE', 'ENE', 'EtN',
         'E', 'EtS', 'ESE', 'SEtE', 'SE', 'SEtS', 'SSE', 'StE',
         'S', 'StW', 'SSW', 'SWtS', 'SW', 'SWtW', 'WSW', 'WtS',
         'W', 'WtN', 'WNW', 'NWtW', 'NW', 'NWtN', 'NNW', 'NtW']
    _wdd = [x*11.25 for x in range(len(_wdl))]
    WIND_DIRECTION = dict(zip(_wdl, _wdd))

    query_template = "INSERT INTO meteo (ts, st_id, p, t_in, h_in, t_out, h_out, d_point, w_chill, w_spd, w_dir, r_rate) VALUES ('{}', {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})"


    def __del__(self):
        self.cur.close()
        self.con.close()
        self.input_file.close()


    def run(self):
        """do the stuff"""
        # process lines
        print("Заливаем данные в базу...", end="")
        for line in self.input_file:
            self.processLine(line)
            print(".", end="")
            sys.stdout.flush()
        print("OK.")


    def processLine(self, line):
        line = line.split()
        fl = self.to_float
        abs_pres = fl(line[0])
        in_t = fl(line[1], ofl_val=-10.0)
        in_h = fl(line[2])
        out_t = fl(line[3], ofl_val=-30.0)
        out_h = fl(line[4])
        dewpoint = fl(line[5])
        windchill = fl(line[6])
        wind_speed = fl(line[7], ofl_val=50.0)
        if not self.meters_pers_second and wind_speed != "null" and wind_speed != 50.0:
            wind_speed = round(wind_speed/3.6, 2)
        wind_dir = self.windDirection(line[8])
        rain_total = fl(line[9])
        timestamp = datetime.strptime(
            "{} {}".format(line[10], line[11]),
            "%H:%M %d.%m.%Y").strftime(
            "%Y-%m-%d %H:%M")

        query = self.query_template.format(timestamp, self.st_id, abs_pres, in_t, in_h, out_t, out_h, dewpoint, windchill, wind_speed, wind_dir, rain_total)
        try:
            self.cur.execute(query)
        except self.con.IntegrityError as e:
            print("Dublicate primary key: {} {}".format(e, query)) # Ignore this error
        except self.con.InternalError as e:       # uncommited data on previus step
            print("uncommited data {} {}".format(e, query))


    def to_float(self, string, null_val = 'null', ofl_val = 'null'):
        if string == '---':
            return null_val
        if string == 'OFL':
            return ofl_val
        return float(string.replace(",", "."))


    def windDirection(self, value):
        if value == '---':
            return "null"
        return self.WIND_DIRECTION[value]



if __name__ == '__main__':
    if len(sys.argv) == 2:
        TxtToDbLoader(sys.argv[1]).run()
    elif len(sys.argv) == 3:
        TxtToDbLoader(sys.argv[1], sys.argv[2]).run()
    else:
        print(__doc__)
        exit(0)

