#!/usr/bin/env python3
"""\
Программа для заливки метео данных из текстового месячного файла в базу данных.
./monthly2db.py file
file - путь к txt файлу;
Автор fedr. Версия 2-20150408.
"""
import psycopg2 as db
import sys
from datetime import datetime
from math import log

class TxtToDbLoader(object):
    def __init__(self, filepath):
        print("Открываем файл... ", end="")
        self.input_file = open(filepath, "rt", errors="ignore")
        print("ОК.")

        # define station
        print("Определяем станцию... ", end="")
        sys.stdout.flush()
        self.st_id = {"150": 3, "148": 4}[filepath[-3:]]
        print("ОК.")

        print("Подключаемся к базе... ", end="")
        sys.stdout.flush()
        self.con = db.connect(host="", database="", user="", password="")
        print("ОК.")
        self.con.autocommit = True
        self.cur = self.con.cursor()


    query_template = "INSERT INTO meteo (ts, st_id, p, t_in, h_in, t_out, h_out, d_point, w_chill, w_spd, w_dir, r_rate) VALUES ('{}', {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})"


    def __del__(self):
        self.cur.close()
        self.con.close()
        self.input_file.close()


    def mmhg2hpa(self, value):
        return round(value * 133.322 * 0.01, 1)

    def pressure(self, value):
        if type(value) == str:
            return value
        if value > 850:
            return value
        else:
            return self.mmhg2hpa(value)


    def run(self):
        """do the stuff"""
        # process lines
        print("Заливаем данные в базу...", end="")
        lines_count = len(self.input_file.readlines())
        self.input_file.seek(0)
        progress = ProgressBar(lines_count, 50)
        count = 0
        progress.replot(count)
        for line in self.input_file:
            self.processLine(line)
            count += 1
            if count % 10 == 0 or count == lines_count:
                progress.replot(count)
        print("OK.")


    def processLine(self, line):
        if self.st_id == 3:
            self.processLine150(line)
        elif self.st_id == 4:
            self.processLine148(line)


    def processLine150(self, line):
        line = line.split()
        if len(line) != 8: return
        fl = self.to_float
        abs_pres = self.pressure(fl(line[7]))
        in_t = "null"
        in_h = "null"
        out_t = fl(line[2])
        out_h = fl(line[3])
        if out_h == 0.0:
            out_h = "null"
        wind_speed = fl(line[5])
        wind_dir = fl(line[6])
        rain_total = fl(line[4])
        timestamp = datetime.strptime(
            "{} {}".format(line[0], line[1]),
            "%Y.%m.%d %H:%M:%S").strftime(
            "%Y-%m-%d %H:%M:%S")
        dewpoint = self.calcDewpoint(out_t, out_h)
        windchill = self.calcWindchill(out_t, wind_speed)

        query = self.query_template.format(timestamp, self.st_id, abs_pres, in_t, in_h, out_t, out_h, dewpoint, windchill, wind_speed, wind_dir, rain_total)
        try:
            self.cur.execute(query)
        except self.con.IntegrityError as e:
            print("Dublicate primary key: {} {}".format(e, query)) # Ignore this error
        except self.con.InternalError as e:       # uncommited data on previus step
            print("uncommited data {} {}".format(e, query))


    def processLine148(self, line):
        line = line.split()
        if len(line) != 10: return
        fl = self.to_float

        abs_pres = self.pressure(fl(line[6]))
        in_t = "null"
        in_h = "null"
        out_t = fl(line[2])
        out_h = fl(line[3])
        wind_speed = fl(line[4])
        wind_dir = fl(line[5])
        rain_total = fl(line[7])
        try:
            timestamp = datetime.strptime(
            "{} {}".format(line[0], line[1]),
            "%Y.%m.%d %H:%M:%S").strftime(
            "%Y-%m-%d %H:%M:%S")
        except:
            timestamp = datetime.strptime(
            "{} {}".format(line[0], line[1]),
            "%Y.%m.%d %H:%M").strftime(
            "%Y-%m-%d %H:%M:%S")
        dewpoint = self.calcDewpoint(out_t, out_h)
        windchill = self.calcWindchill(out_t, wind_speed)

        query = self.query_template.format(timestamp, self.st_id, abs_pres, in_t, in_h, out_t, out_h, dewpoint, windchill, wind_speed, wind_dir, rain_total)
        try:
            self.cur.execute(query)
        except self.con.IntegrityError as e:
            print("Dublicate primary key: {} {}".format(e, query)) # Ignore this error
        except self.con.InternalError as e:       # uncommited data on previus step
            print("uncommited data {} {}".format(e, query))



    def to_float(self, string, null_val = 'null', ofl_val = 'null'):
        if string == '---':
            return null_val
        if string == 'OFL':
            return ofl_val
        return float(string.replace(",", "."))


    def calcDewpoint(self, t, rh):
        """Вычисляем точку росы. t - температура, rh - относит. влажность."""
        if t is None or rh == "null":
            return "null"
        dew =  243.04*(log(rh/100)+((17.625*t)/(243.04+t)))/(17.625-log(rh/100)-((17.625*t)/(243.04+t)))
        return str(round(dew, 1))


    def calcWindchill(self, t, v):
        """Вычисляем windchill, t - температура, v - скорость ветра."""
        if t is None or v is None:
          return "null"
        if t <= 10.0 and v > 4/3:
          wc = 13.127+0.6215*t-13.947*v**0.16+0.486*t*v**0.16
          return str(round(wc, 1))
        return str(t)




class ProgressBar(object):
    # [###..] 63%
    def __init__(self, max, len):
        self.max = max
        self.len = len


    def getString(self, value):
        proc = int(value/self.max*100)
        done = int(value/self.max * self.len)
        s = " [{}] {:2d}% ".format(("#"*done + "."*(self.len-done)), proc)
        return s


    def replot(self, value):
        print("\r", end="")
        print(self.getString(value), end="")
        sys.stdout.flush()





if __name__ == '__main__':
    argc = len(sys.argv)
    if argc == 2:
        TxtToDbLoader(sys.argv[1]).run()
    else:
        print(__doc__)
        exit(0)

