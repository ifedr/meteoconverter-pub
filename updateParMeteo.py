#!/usr/bin/env python3
"""\
Программа для определения изменений(!) и заливки метео данных из текстовых месячных файлов .148/.150 в базу данных.
Автор fedr. Версия 1-20150408.
"""

from monthly2db import TxtToDbLoader
import time
import glob
import os


class ParatunkaMeteoUpdater(object):
    LAST_CHECK_FNAME = ".last_check.par"


    def __init__(self):
        self.readLastCheck()



    def readLastCheck(self):
        try:
            self.last_check = float(open(self.LAST_CHECK_FNAME).read())
        except:
            self.last_check = 0.0


    def saveLastCheck(self):
        now = time.time()
        open(self.LAST_CHECK_FNAME, "wt").write(str(now))
        print("Сохранил текущее время в {}".format(self.LAST_CHECK_FNAME))


    def run(self, path_mask):
        fnames = self.getModifiedFiles(path_mask)
        for fname in fnames:
            print(fname)
            TxtToDbLoader(fname).run()


    def getModifiedFiles(self, path_mask):
        res = []
        for fname in sorted(glob.glob(path_mask)):
            if os.path.getmtime(fname) > self.last_check:
                res.append(fname)
        return res


if __name__ == '__main__':
    updater = ParatunkaMeteoUpdater()
    updater.run("/raid/common/Data/Paratunka-station/Meteo/par_month/148/ik*.148")
    updater.run("/raid/common/Data/Paratunka-station/Meteo/par_month/148/*/ik*.148")
    updater.run("/raid/common/Data/Paratunka-station/Meteo/par_month/150/ik*.150")
    updater.run("/raid/common/Data/Paratunka-station/Meteo/par_month/150/*/ik*.150")
    updater.saveLastCheck()
