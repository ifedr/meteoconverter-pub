#!/bin/bash

# Если экземпляр скрипта уже запущен, то убить его.
PIDFILE=/tmp/update-meteo.pid
if [ -e $PIDFILE ] && kill -9 -`cat $PIDFILE`; then
    echo "* Убил уже запущенный экзмепляр скрипта."
else
    echo "* Экземпляры скрипта сейчас не запущены."
fi
# make sure the lockfile is removed when we exit and then claim it
trap "rm -f $PIDFILE; exit" INT TERM EXIT
echo $$ > $PIDFILE
# ========================


echo "-----`date`-----"


echo "=== МИКИЖА ==="
MIK_METEO=/raid/common/Data/Mikizha-station/Meteo

cd mik

echo "* Синхронизируем history.dat"
rsync --update --verbose user@192.168..:/home/lai/wine_c/HeavyWeather/history.dat history.dat
echo ""

echo "* Переименовываем history.dat"
./meteoconverter.py history.dat --mode rename --station mik --quiet --path-dat $MIK_METEO/data --path-txt $MIK_METEO/txt
echo ""

echo "* конвертируем в тхт"
./meteoconverter.py history.dat.bak --mode txt --output history.txt --station mik --quiet
echo "* заливка в БД"
./meteoconverter.py history.dat.bak --mode db --station mik --quiet --continue-from-last
echo "* месячный файл"
./meteoconverter.py history.dat.bak --mode monthly --output $MIK_METEO/mik_month --station mik --quiet --continue-from-last
echo "* суточный met2"
./meteoconverter.py history.dat.bak --mode met2 --output $MIK_METEO/met2 --station mik --quiet --continue-from-last
echo "* суточный met"
./meteoconverter.py history.dat.bak --mode met --output $MIK_METEO/mik_day --station mik --quiet --continue-from-last --save-last
echo ""

cd ..

#exit 0

echo "=== КАРЫМШИНА ==="
KAR_METEO=/raid/common/Data/Karimshino-station/Meteo

cd kar

echo "* Синхронизируем history.dat"
rsync --update --verbose user@192.168..:/home/lai/wine_c/HeavyWeather/history.dat history.dat 
echo ""

echo "* Переименовываем history.dat"
./meteoconverter.py history.dat --mode rename --station kar --quiet --path-dat $KAR_METEO/data --path-txt $KAR_METEO/txt
echo ""

echo "* конвертируем в тхт"
./meteoconverter.py history.dat.bak --mode txt --output history.txt --station kar --quiet
echo "* заливка в БД"
./meteoconverter.py history.dat.bak --mode db --station kar --quiet --continue-from-last
echo "* месячный файл"
./meteoconverter.py history.dat.bak --mode monthly --output $KAR_METEO/kar_month --station kar --quiet --continue-from-last
echo "* суточный met2"
./meteoconverter.py history.dat.bak --mode met2 --output $KAR_METEO/met2 --station kar --quiet --continue-from-last
echo "* суточный met"
./meteoconverter.py history.dat.bak --mode met --output $KAR_METEO/kar_day --station kar --quiet --continue-from-last --save-last
echo ""

cd ..



echo "-----`date`-----"

