#!/usr/bin/env python3
"""\
Программа конвертациии бинарного файла с метеоданными history.dat в текстовые форматы.
Автор: fedr. Версия: 8-20150312.
© ИКИР ДВО РАН, 2015.
"""
import sys
import os
import struct
from datetime import datetime, timedelta
from math import log, sqrt



class Converter(object):
  def __init__(self, input_fpath, timezone=0):
    try:
      self.input_file = open(input_fpath, 'rb')
      self.timezone = timezone
    except:
      print("Не могу открыть входной файл: %s" % input_fpath)
      exit(0)


  def _saveLastRecord(self, data):
    if data is None:
      return
    print("Сохраняю последнюю запись в .last_record...")
    with open(".last_record", "wb") as f:
      f.write(data)


  def _seekAfterLastRecord(self):
    """Устанавливает курсор в файле после записи, сохраненной в .last_record"""
    pos = self.input_file.tell()
    try:
      last_record = open(".last_record", "rb").read()
      self.input_file.seek(0)
      while True:
        data = self.input_file.read(Record.RECORD_LEN)
        if len(data) < Record.RECORD_LEN: # Конец файла
          self.input_file.seek(pos)
          return pos
        if data == last_record:
          return self.input_file.tell()
    except IOError:
      pass
    return pos


  def saveAsTxt(self, filepath=None):
    """Сохраняем в текстовый файл, в том же формате, что использовался раньше."""
    txt_header = "\n".join([
      "\t".join(["Absolute Pressure", "Indoor Temperature",
        "Indoor Humidity", "Outdoor Temperature",
        "Outdoor Humidity", "Dewpoint", "Windchill",
        "Wind Speed", "Wind Direction", "Rain Total",
        "Time", "Date"]),
      "\t".join(["[hPa]", "[°C]", "[%]", "[°C]", "[%]", "[°C]", "[°C]", "[m/s]", "[mm]"])
      ])
    if filepath is None:
      f = sys.stdout
    else:
      f = open(filepath, 'w')
    # Header:
    f.write(txt_header)
    f.write("\n")
    # Content:
    while True:
      data = self.input_file.read(Record.RECORD_LEN)
      if len(data) < Record.RECORD_LEN:
        # Tail(data)
        break
      record = Record(data, self.timezone)
      s = record.toTxtString()
      f.write(s)
      f.write("\n")
    f.close()



  def saveAsMet(self, path=None):
    """Сохраняем в виде суточных текстовых файликов в папку path с шагом 1 минута (для Мишиной программы)."""
    # Чтобы использовать os.path.join
    if path is None:
      path = ""

    output_fname = None
    met_file = None

    if args.continue_from_last:
      self._seekAfterLastRecord()

    # считываем входной файл
    while True:
      data = self.input_file.read(Record.RECORD_LEN)
      if len(data) < Record.RECORD_LEN:
        if args.save_last:
          self.input_file.seek(self.input_file.tell()-len(data)-Record.RECORD_LEN)
          last_rec = self.input_file.read(Record.RECORD_LEN)
          self._saveLastRecord(last_rec)
        # Tail(data)
        break

      record = Record(data, self.timezone)
      new_output_fname = record.timestamp.strftime("mi%y%m%d.met")
      # Если имя выходного файла изменилось (дата изменилась)
      if new_output_fname != output_fname:
        output_fpath = os.path.join(path, new_output_fname)
        # Если файл уже существует
        if os.path.exists(output_fpath):
          if not args.continue_from_last:
            print("{} -- пропущен".format(output_fpath))
            output_fname = new_output_fname
            continue
          met_file = MetFile(record.timestamp, output_fpath)
          met_file.loadFromFile()
          print("{} -- открыт для внесения изменений".format(output_fpath))
        else:
          met_file = MetFile(record.timestamp, output_fpath)
      if met_file:
        met_file.setLine(record)
      output_fname = new_output_fname


  def saveAsMet2(self, path=None):
    """Сохраняем в виде суточных текстовых файликов в папку path в новом формате."""
    if path is None:
      path = ""
    output_fname = None
    met_file = None

    if args.continue_from_last:
      self._seekAfterLastRecord()

    # считываем входной файл
    while True:
      data = self.input_file.read(Record.RECORD_LEN)
      if len(data) < Record.RECORD_LEN:
        if args.save_last:
          self.input_file.seek(self.input_file.tell()-len(data)-Record.RECORD_LEN)
          last_rec = self.input_file.read(Record.RECORD_LEN)
          self._saveLastRecord(last_rec)
        # Tail(data)
        break

      record = Record(data, self.timezone)
      new_output_fname = "{station}_{date}.met2".format(
        station=args.station,
        date=record.timestamp.strftime("%Y%m%d")
        )
      # Если имя выходного файла изменилось (дата изменилась)
      if new_output_fname != output_fname:
        output_fpath = os.path.join(path, new_output_fname)
        # Если файл уже существует
        if os.path.exists(output_fpath):
          if not args.continue_from_last:
            print("{} -- пропущен".format(output_fpath))
            output_fname = new_output_fname
            continue
          met_file = Met2File(record.timestamp, output_fpath)
          met_file.loadFromFile()
          print("{} -- открыт для внесения изменений".format(output_fpath))
        else:
          met_file = Met2File(record.timestamp, output_fpath)
      if met_file:
        met_file.appendRecord(record)
      output_fname = new_output_fname


  def saveAsMonthly(self, path=None):
    """Сохраняем в виде месячных текстовых файликов в папку path в формате другой лаборатории."""
    if path is None:
      path = ""
    output_fname = None
    out_file = None

    if args.continue_from_last:
      self._seekAfterLastRecord()

    # считываем входной файл
    while True:
      data = self.input_file.read(Record.RECORD_LEN)
      if len(data) < Record.RECORD_LEN:
        if args.save_last:
          self.input_file.seek(self.input_file.tell()-len(data)-Record.RECORD_LEN)
          last_rec = self.input_file.read(Record.RECORD_LEN)
          self._saveLastRecord(last_rec)
        # Tail(data)
        break

      record = Record(data, self.timezone)

      new_output_fname = "{station}{date}.{extention}".format(
        station={1: "mik", 2: "krm"}[global_station_id],
        date=record.timestamp.strftime("%y%m"),
        extention={1: "749", 2: "748"}[global_station_id]
        )
      # Если имя выходного файла изменилось (дата изменилась)
      if new_output_fname != output_fname:
        output_fpath = os.path.join(path, new_output_fname)
        # Если файл уже существует
        if os.path.exists(output_fpath):
          if not args.continue_from_last:
            print("{} -- пропущен".format(output_fpath))
            output_fname = new_output_fname
            continue
          out_file = open(output_fpath, "at")
          print("{} -- открыт для внесения изменений".format(output_fpath))
        else:
          out_file = open(output_fpath, "at")
      if out_file:
        out_file.write(record.toMonthFormatString() + "\n")
      output_fname = new_output_fname



  def saveAsSql(self, filepath=None):
    """Сохраняем в виде sql-запроса к БД Альберта."""
    if filepath is None:
      f = sys.stdout
    else:
      f = open(filepath, 'w')

    if args.continue_from_last:
      self._seekAfterLastRecord()

    # считываем входной файл
    while True:
      data = self.input_file.read(Record.RECORD_LEN)
      if len(data) < Record.RECORD_LEN:
        if args.save_last:
          self.input_file.seek(self.input_file.tell()-len(data)-Record.RECORD_LEN)
          last_rec = self.input_file.read(Record.RECORD_LEN)
          self._saveLastRecord(last_rec)
        # Tail(data)
        break

      record = Record(data, self.timezone)
      s = record.toSqlString() + ";"
      f.write(s)
      f.write("\n")



  def uploadToDb(self):
    """Загружаем файл в базу данных Альберта"""
    import psycopg2 as db
    con = db.connect(host="", database="", user="", password="")
    con.autocommit = True
    cur = con.cursor()

    if args.continue_from_last:
      self._seekAfterLastRecord()

    # считываем входной файл
    while True:
      data = self.input_file.read(Record.RECORD_LEN)
      if len(data) < Record.RECORD_LEN:
        if args.save_last:
          self.input_file.seek(self.input_file.tell()-len(data)-Record.RECORD_LEN)
          last_rec = self.input_file.read(Record.RECORD_LEN)
          self._saveLastRecord(last_rec)
        # Tail(data)
        break

      record = Record(data, self.timezone)
      query = record.toSqlString()
      try:
        cur.execute(query)
      except con.IntegrityError as e:
          print("Dublicate primary key: {}".format(e)) # Ignore this error
      except con.InternalError as e:       # uncommited data on previus step
          print("uncommited data {} {}".format(e, query))
    cur.close()
    con.close()



  def info(self):
    self.input_file.seek(-Tail.TAIL_LEN, 2)
    data = self.input_file.read(Tail.TAIL_LEN)
    tail = Tail(data)
    msg = "{rows} записей с {begin} по {end}.".format(
      rows=tail.rows, begin=tail.timestamp_begin, end=tail.timestamp_end)
    print(msg)



class Record(object):
  """Класс записи метеоданных из history.dat"""
  RECORD_LEN = 36 # bytes
  record_struct = struct.Struct("<LLffLfffHH")
  WIND_DIRECTIONS_LETTERS = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
  WIND_DIRECTIONS_DEGREES = [ 22.5*x for x in range(16) ]
  # Диапазоны корректных значений, взяты из документации
  MIN_OUTDOOR_TEMPERATURE = -29.9
  MAX_OUTDOOR_TEMPERATURE = 69.900002
  MIN_INDOOR_TEMPERATURE = -9.9
  MAX_INDOOR_TEMPERATURE = 59.900002
  MIN_HUMIDITY = 1
  MAX_HUMIDITY = 99
  MIN_WIND_SPEED = 0.0
  MAX_WIND_SPEED = 50.0
  MIN_PRESSURE = 300.0
  MAX_PRESSURE = 1099.0


  @property
  def abs_pressure(self):
    return self._abs_pressure
  @abs_pressure.setter
  def abs_pressure(self, value):
    if self.MIN_PRESSURE <= value <= self.MAX_PRESSURE:
      self._abs_pressure = value
    else:
      self._abs_pressure = None

  @property
  def wind_speed(self):
    return self._wind_speed
  @wind_speed.setter
  def wind_speed(self, value):
    if self.MIN_WIND_SPEED <= value <= self.MAX_WIND_SPEED:
      self._wind_speed = value
    else:
      self._wind_speed = None

  @property
  def indoor_temp(self):
    return self._indoor_temp
  @indoor_temp.setter
  def indoor_temp(self, value):
    if self.MIN_INDOOR_TEMPERATURE <= value <= self.MAX_INDOOR_TEMPERATURE:
      self._indoor_temp = value
    else:
      self._indoor_temp = None

  @property
  def outdoor_temp(self):
    return self._outdoor_temp
  @outdoor_temp.setter
  def outdoor_temp(self, value):
    if self.MIN_OUTDOOR_TEMPERATURE <= value <= self.MAX_OUTDOOR_TEMPERATURE:
      self._outdoor_temp = value
    else:
      self._outdoor_temp = None

  @property
  def indoor_humidity(self):
    return self._indoor_humidity
  @indoor_humidity.setter
  def indoor_humidity(self, value):
    if self.MIN_HUMIDITY <= value <= self.MAX_HUMIDITY:
      self._indoor_humidity = value
    else:
      self._indoor_humidity = None

  @property
  def outdoor_humidity(self):
    return self._outdoor_humidity
  @outdoor_humidity.setter
  def outdoor_humidity(self, value):
    if self.MIN_HUMIDITY <= value <= self.MAX_HUMIDITY:
      self._outdoor_humidity = value
    else:
      self._outdoor_humidity = None

  @property
  def timestamp(self):
    return self._timestamp
  @timestamp.setter
  def timestamp(self, value):
    self._timestamp = self.from1900(value)


  def __init__(self, data, timezone=0):
    if len(data) != self.RECORD_LEN:
      raise Exception("Ошибка: длина записи отлична от %d" % self.RECORD_LEN)
    unpacked = self.record_struct.unpack(data)
    # self.unknown = unpacked[0]
    self.timestamp = unpacked[1]
    self.abs_pressure = unpacked[2]
    self.wind_speed = unpacked[3]
    self.wind_direction = unpacked[4]
    self.total_rainfall = unpacked[5]
    self.indoor_temp = unpacked[6]
    self.outdoor_temp = unpacked[7]
    self.indoor_humidity = unpacked[8]
    self.outdoor_humidity = unpacked[9]
    self.dewpoint = self._calcDewpoint(self.outdoor_temp, self.outdoor_humidity)
    self.windchill = self._calcWindchill(self.outdoor_temp, self.wind_speed)
    if timezone:
      self._correctTime(timezone)


  def _calcDewpoint(self, t, rh):
    """Вычисляем точку росы. t - температура, rh - относит. влажность."""
    if t is None or rh is None:
      return None
    return 243.04*(log(rh/100)+((17.625*t)/(243.04+t)))/(17.625-log(rh/100)-((17.625*t)/(243.04+t)))

  def _calcDewpointNikita(self, t, rh):
    """Вычисляем точку росы. t - температура, rh - относит. влажность."""
    if t is None or rh is None:
      return None
    a = 17.270000457763672
    b = 237.69999694824219
    rh /= 100.0
    dewpoint = b * (a * t / (b + t) + log(rh)) / (a - (a * t / (b + t) + log(rh)))
    return dewpoint


  def _calcWindchill(self, t, v):
    """Вычисляем windchill, t - температура, v - скорость ветра."""
    if t is None or v is None:
      return None
    if t <= 10.0 and v > 4/3:
      return 13.127+0.6215*t-13.947*v**0.16+0.486*t*v**0.16
    return t

  def _calcWindchillNikita(self, t, v):
    """Вычисляем windchill, t - температура, v - скорость ветра."""
    if t is None or v is None:
      return None
    if v < 0.01:
      return t
    v *= 3.6
    w = 13.119999999999999 + 0.6215000000000001 * t - 11.369999999999999 * v**0.16 + 0.3965 * t * v**0.16
    return w


  def _correctTime(self, timezone):
    """Переводит время в часовой пояс UTC+0."""
    delta = timedelta(hours=timezone)
    self._timestamp -= delta


  @staticmethod
  def from1900(value):
    """Вход - число секунд с 1900 года (в стандартной библиотеке с 1970)."""
    epoch = datetime(1900, 1, 1, 0, 0, 0)
    delta = timedelta(seconds=value)
    result = epoch + delta
    return result


  def toTxtString(self):
    """Возвращает строковое представление записи в формате, в установленном раннее формате (программа Никиты)."""
    f2s = lambda x: "{0:.01f}".format(x) if x is not None else "---"
    f2sOFL = lambda x: "{0:.01f}".format(x) if x is not None else "OFL"
    i2s = lambda x: str(x) if x is not None else "---"
    li = [
      f2s(self.abs_pressure),
      f2sOFL(self.indoor_temp),
      i2s(self.indoor_humidity),
      f2sOFL(self.outdoor_temp),
      i2s(self.outdoor_humidity),
      f2s(self.dewpoint),
      f2s(self.windchill),
      f2s(self.wind_speed),
      self.WIND_DIRECTIONS_LETTERS[self.wind_direction] if self.wind_speed is not None else "---",
      f2s(self.total_rainfall),
      self.timestamp.strftime("%H:%M\t%d.%m.%Y")
    ]
    return "\t".join(li)


  def toMetString(self):
    """Возвращает строковое представление записи в формате, в установленном раннее формате (программа Миши)."""
    f2s = lambda x: "{0:.01f}".format(x) if x is not None else "0.0"
    i2s = lambda x: str(x) if x is not None else "0.0"
    li = [
      self.timestamp.strftime("%H:%M:%S"),
      f2s(self.abs_pressure),
      "{0:.01f}".format(self.indoor_temp) if self.indoor_temp is not None else "-10.0",
      i2s(self.indoor_humidity),
      "{0:.01f}".format(self.outdoor_temp) if self.outdoor_temp is not None else "-30.0",
      i2s(self.outdoor_humidity),
      f2s(self.dewpoint),
      f2s(self.windchill),
      f2s(self.wind_speed),
      f2s(self.WIND_DIRECTIONS_DEGREES[self.wind_direction]),
      f2s(self.total_rainfall)
    ]
    return "\t".join(li)


  def toMet2String(self):
    """Возвращает строковое представление записи в новом текстовом формате."""
    f2s = lambda x: "{0:.01f}".format(x) if x is not None else "---"
    i2s = lambda x: str(x) if x is not None else "---"
    li = [
      self.timestamp.strftime("%H:%M"),
      f2s(self.abs_pressure),
      "{0:.01f}".format(self.indoor_temp) if self.indoor_temp is not None else "-10.0",
      i2s(self.indoor_humidity),
      "{0:.01f}".format(self.outdoor_temp) if self.outdoor_temp is not None else "-30.0",
      i2s(self.outdoor_humidity),
      f2s(self.dewpoint),
      f2s(self.windchill),
      f2s(self.wind_speed),
      f2s(self.WIND_DIRECTIONS_DEGREES[self.wind_direction]),
      f2s(self.total_rainfall)
    ]
    return "\t".join(li)


  def toSqlString(self):
    """Возвращает строковое представление записи в формате для составления sql-запроса в базу данных."""
    # meteo (ts, st_id, p, t_in, h_in, t_out, h_out, d_point, w_chill, w_spd, w_dir, r_rate)
    # Что делать с --- ?
    query = "INSERT INTO meteo (ts, st_id, p, t_in, h_in, t_out, h_out, d_point, w_chill, w_spd, w_dir, r_rate) VALUES ({})"
    f2s = lambda x: "{0:.01f}".format(x) if x is not None else "null"
    i2s = lambda x: str(x) if x is not None else "null"
    li = [
      "'{}'".format(self.timestamp.strftime("%Y-%m-%d %H:%M:%S")),
      str(global_station_id),
      f2s(self.abs_pressure),
      "{0:.01f}".format(self.indoor_temp) if self.indoor_temp is not None else "-10.0",
      i2s(self.indoor_humidity),
      "{0:.01f}".format(self.outdoor_temp) if self.outdoor_temp is not None else "-30.0",
      i2s(self.outdoor_humidity),
      f2s(self.dewpoint),
      f2s(self.windchill),
      f2s(self.wind_speed),
      f2s(self.WIND_DIRECTIONS_DEGREES[self.wind_direction]),
      f2s(self.total_rainfall)
    ]
    return query.format(", ".join(li))


  def toMonthFormatString(self):
    """Возвращает строковое представление записи в формате, в котором хранятся месячные данные для другой лаборатории."""
    f2s = lambda x: "{0:.01f}".format(x) if x is not None else "---"
    f2s2 = lambda x: "{0:.02f}".format(x) if x is not None else "---"
    f2sOFL = lambda x: "{0:.01f}".format(x) if x is not None else "OFL"
    li = [
      self.timestamp.strftime("%Y.%m.%d    %H:%M:%S"),
      f2sOFL(self.outdoor_temp),
      f2s(self.outdoor_humidity),
      f2s(self.total_rainfall),
      f2s2(self.wind_speed),
      f2s(self.WIND_DIRECTIONS_DEGREES[self.wind_direction]),
      f2s(self.abs_pressure)
    ]
    return "\t".join(li)


class Tail(object):
  """28 байт в конце файла с информацией о файле."""
  TAIL_LEN = 28 # bytes
  tail_struct = struct.Struct("<LLfLLLL")

  def __init__(self, data):
    if len(data) != self.TAIL_LEN:
      raise Exception("Ошибка: длина хвоста файла отлична от %d" % self.TAIL_LEN)
    unpacked = self.tail_struct.unpack(data)
    self.rows = unpacked[4]
    self.timestamp_begin = Record.from1900(unpacked[5])
    self.timestamp_end = Record.from1900(unpacked[6])



class MetFile:
  """Класс, содержащий строки с шагом в 1 минуту."""
  empty_line = "\t".join(["0.0"]*8+["0"]+["0.0"]) + "\n"

  def __init__(self, timestamp, filename=None):
    if filename:
      self.filename = filename
    else:
      self.filename = timestamp.strftime("%Y%m%d.met")
    # Заполняем lines "пустыми" строками
    self.lines = []
    ts = datetime(timestamp.year, timestamp.month, timestamp.day)
    delta = timedelta(minutes=1)
    while ts.date() == timestamp.date():
      self.lines.append("\t".join([ts.strftime("%H:%M:%S"), self.empty_line]))
      ts += delta


  def __del__(self):
    self.save()


  def save(self):
    with open(self.filename, "w") as f:
      f.writelines(self.lines)
    print("{} -- сохранён".format(self.filename))


  def setLine(self, record):
    n = 60 * record.timestamp.hour + record.timestamp.minute
    self.lines[n] = record.toMetString() + "\n"


  def loadFromFile(self):
    try:
      f = open(self.filename, "rt")
      self.lines = f.readlines()
    except IOError:
      pass


class Met2File:
  """Новый текстовый суточный формат файла (от февраля 2014)."""
  def __init__(self, timestamp, filename=None):
    if filename:
      self.filename = filename
    else:
      self.filename = "{station}_{date}.met2".format(
        station=args.station,
        date=timestamp.strftime("%Y%m%d")
        )
    # Заполняем lines "пустыми" строками
    self.header = "\n".join([
      os.path.basename(self.filename),
      "\t".join(["Time", "Absolute Pressure", "Indoor Temperature",
        "Indoor Humidity", "Outdoor Temperature",
        "Outdoor Humidity", "Dewpoint", "Windchill",
        "Wind Speed", "Wind Direction", "Rain Total"]),
      "\t".join(["[HH:MM]","[hPa]", "[°C]", "[%]", "[°C]", "[%]", "[°C]", "[°C]", "[m/s]", "[°]", "[mm]"]),
      "\n"
      ])
    self.lines = []



  def __del__(self):
    self.save()


  def save(self):
    with open(self.filename, "w") as f:
      f.write(self.header)
      f.writelines(self.lines)
    print("{} -- сохранён".format(self.filename))


  def appendRecord(self, record):
    self.lines.append(record.toMet2String() + "\n")


  def loadFromFile(self):
    try:
      f = open(self.filename, "rt")
      # Пропускаем заголовок до пустой строки
      while f.readline() != "\n":
        pass
      self.lines = f.readlines()
    except IOError:
      pass



def rename(new_fpath, old_fpath=None):
  """Сравнивает первую запись в старом и новом файле history.dat,
  и переименовывает файлы."""
  if old_fpath is None:
    old_fpath = new_fpath + ".bak"
  print("Сравниваю первую запись в файлах:\n{} и {}".format(new_fpath, old_fpath))
  same = False
  if os.path.exists(old_fpath):
    old_file = open(old_fpath, "rb")
    new_file = open(new_fpath, "rb")
    size = Record.RECORD_LEN
    if old_file.read(size) == new_file.read(size):
      same = True
      print("Одинаковые (файл продолжается).")
    else:
      same = False
      print("Разные (начат новый файл).")
    old_file.close()
    new_file.close()
  if not same and os.path.exists(old_fpath):
    f = open(old_fpath, "rb")
    f.seek(-Tail.TAIL_LEN, 2) # отступать с конца файла
    data = f.read(Tail.TAIL_LEN)
    f.close()
    tail = Tail(data)
    fname = "{station}_{begin}-{end}.dat".format(
      station=args.station,
      begin=tail.timestamp_begin.strftime("%Y%m%d.%H%M"),
      end=tail.timestamp_end.strftime("%Y%m%d.%H%M"))
    os.rename(old_fpath, fname)
    print("Переименовал {} в {}".format(old_fpath, fname))

    print("Обсчитываем {} в txt".format(fname))
    fnametxt = fname[:-3] + "txt"
    Converter(fname, 0).saveAsTxt(fnametxt)

    if args.path_dat:
      pathdat = os.path.join(args.path_dat, fname)
      os.rename(fname, pathdat)
      print("Переместил {} в {}".format(fname, pathdat))

    if args.path_txt:
      pathtxt = os.path.join(args.path_txt, fnametxt)
      os.rename(fnametxt, pathtxt)
      print("Переместил {} в {}".format(fnametxt, pathtxt))

  os.rename(new_fpath, old_fpath)
  print("Переименовал {} в {}".format(new_fpath, old_fpath))




def test():
  pass



if __name__ == "__main__":
  STATIONS = {"mik": 1, "kar": 2, "par": 3}
  import argparse
  if not "--quiet" in sys.argv:
    print(__doc__)
  argparser = argparse.ArgumentParser()
  argparser.add_argument("input", help="Входной файл метеоданных в формате history.dat")
  argparser.add_argument("-o", "--output", help="Выходной файл / Путь для выходных файлов (при mode met).")
  argparser.add_argument("-t", "--timezone", help="Часовой пояс в формате '+12'", type=int, default=0)
  argparser.add_argument("-m", "--mode", help="Выбор режима работы", choices=["txt", "met", "met2", "sql", "monthly", "db", "rename", "info"], required=True)
  argparser.add_argument("--station", help="Выбор станции. Необходимо указать при mode sql.", choices=list(STATIONS.keys()))
  argparser.add_argument("--continue-from-last", help="Продолжить с сохраненной записи.", action="store_true")
  argparser.add_argument("--save-last", help="Сохранить последнюю запись в .last_record, чтобы в след. раз продолжить с этого места.", action="store_true")
  argparser.add_argument("--quiet", help="Тихий режим: не выводить подробную информацию о процессе.", action="store_true")
  argparser.add_argument("--path-dat", help="Путь к директории с dat файлами.")
  argparser.add_argument("--path-txt", help="Путь к директории с txt файлами.")
  args = argparser.parse_args()

  # Определение станции
  if args.station is None:
    for k in STATIONS:
      if k in args.input:
        args.station = k
        break
    if args.station == None:
      print("Невозможно определить станцию по имени файла, необходимо указать явно с помощью ключа --station (смотри --help).")
      exit(0)

  # Вывод используемых аргументов аргументов
  if not args.quiet:
    print("Входной файл: {}".format(args.input))
    print("Часовой пояс: UTC+{:02d}".format(args.timezone))
    print("Режим: {}".format(args.mode))
    if args.mode == "met" or args.mode == "met2":
      print("Путь к выходным файлам: {}".format(args.output or "."))
    else:
      print("Выходной файл: {}".format(args.output or "STDOUT"))
    print("Станция: {}".format(args.station))
    print()

  # Преобразуем символьное название станции в st_id из БД 
  # и помещаем его в глобальную переменную.
  if args.station:
    global global_station_id
    global_station_id = STATIONS[args.station]

  if args.mode == "rename":
    rename(args.input, args.output)
    exit(0)

  converter = Converter(args.input, args.timezone)
  if args.mode == "txt":
    converter.saveAsTxt(args.output)
  elif args.mode == "met":
    converter.saveAsMet(args.output)
  elif args.mode == "met2":
    converter.saveAsMet2(args.output)
  elif args.mode == "sql":
    converter.saveAsSql(args.output)
  elif args.mode == "monthly":
    converter.saveAsMonthly(args.output)
  elif args.mode == "db":
    converter.uploadToDb()
  elif args.mode == "info":
    converter.info()


